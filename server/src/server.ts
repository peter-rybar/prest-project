import { app } from "./app";
import { AddressInfo } from "net";
import * as log4js from "log4js";
import * as http from "http";
// import * as https from "https";
// import * as fs from "fs";

const log = log4js.getLogger("server");

const port = Number(process.env.PORT) || 8080;
const host = process.env.HOST || ((process.env as any).NODE_ENV === "production" ? "0.0.0.0" : "localhost");

const httpServer = http.createServer(app);
httpServer.listen(port, host, () => {
    const addr = httpServer.address() as AddressInfo;
    log.info("listening: http://%s:%s", addr.address, addr.port);
});

// const privateKey = fs.readFileSync(__dirname + "/../ssl/key.pem", "utf8");
// const certificate = fs.readFileSync(__dirname + "/../ssl/cert.pem", "utf8");
// const sslOptions = {
//     key: privateKey,
//     cert: certificate,
//     passphrase: "rybar"
// };
// const httpsServer = https.createServer(sslOptions, app);
// httpsServer.listen(8443, host, () => {
//     const addr = httpsServer.address() as AddressInfo;
//     log.info("listening: https://%s:%s", addr.address, addr.port);
// });
